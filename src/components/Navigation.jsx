import React from "react";
import { Link } from "react-router-dom"; // Importer Link depuis react-router-dom
import "./Navigation.css";

function Navigation() {
  return (
    <ul className="Navigation">
      {/* Utilisez Link avec les bons chemins vers les pages */}
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/produits">Products</Link>
      </li>
      <li>
        <Link to="/panier">Cart</Link>
      </li>
    </ul>
  );
}

export default Navigation;

