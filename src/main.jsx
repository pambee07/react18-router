import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

// TODO install and import react router

import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/panier",
    element: <Cart />,
  },
  {
    path: "/produits",
    element: <ProductList />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* TODO use react router here */}

    <RouterProvider router={router} />
  </React.StrictMode>
);
